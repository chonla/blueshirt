<?php

require "setup/lib/blueshirt.php";
require "setup/builders/builder.php";
require "setup/lib/logger.php";
require "setup/lib/composer.php";
require "setup/lib/downloader.php";
require "setup/lib/codeception.php";

$config = require "setup/config.php";
$blueprint_db = require "setup/blueprints/db.php";
$blueprint_files = require "setup/blueprints/files.php";
$blueprint_dbdata = require "setup/blueprints/dbdata.php";
$blueprint_dependencies = require "setup/blueprints/dependencies.php";
$blueprint_templates = require "setup/blueprints/templates.php";

$installer = new BlueShirt($config);
$logger = new Logger();
$logger->set_level($config["blueshirt"]["log_level"]);

$installer->set_logger($logger);
$installer->initialize();

if ($installer->install()) {
    $logger->info("Exit: All done.");
    $logger->nl();

    $logger->info("You should remove install.php and setup directory after you finished the installation.");

    if ($config["blueshirt"]["install_test"]) {
        $logger->info("If you want to run test suite, start server at " . $config["codeception"]["base_url"] . " and run \"php setup/tmp/codecept.phar run api\".");
    }
}
