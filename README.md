# BlueShirt
-----
Just a RESTful CMS.

## Install

Installation script will initial all necessary stuff. You can custom some configuration. See [Configuration](#configuration) section below for more information.

To run installation script, run the following command.

```sh
php install.php
```

## Configuration

Edit ```setup/config.php``` to change configuration as you need and run installation script.

## Test RESTful service

Start built-in php server by the following command and open your browser to http://localhost:8080/articles to see the result.

```sh
php -S 0.0.0.0:8080
```

## Try it in docker

### PHP5

```
https://github.com/chonla/docker-blueshirt-php5
```

### PHP7

```
https://github.com/chonla/docker-blueshirt
```

## Execute test cases

BlueShirt, by default, comes with Codeception and several test cases. You can execute test cases by running the following command.

```
php setup/tmp/codecept.phar run api
```
