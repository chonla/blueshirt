<?php

class DbBuilder {
    function __construct($opts) {
        $this->logger = $opts["logger"];
        $this->dsn = $opts["dsn"];
    }

    public function build($blueprint) {
        $this->logger->info("Connecting to datbase: " . $this->dsn);
        $this->db = new PDO($this->dsn);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        foreach($blueprint as $item_name => $item_properties) {
            $result = $this->create($item_name, $item_properties);
            if ($result === FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    function create($item_name, $item_properties) {
        $this->logger->info("Creating item: " . $item_name);
        $auto_patched_properties = [
            "id" => "int+*",
            "created_time" => "uint",
            "updated_time" => "uint",
        ];
        $item_properties = array_merge($auto_patched_properties, $item_properties);
        $sql = $this->create_sql($item_name, $item_properties);
        $this->logger->debug("> > with SQL: " . $sql["table"]);
        $result = $this->db->exec($sql["table"]);
        if ($result === FALSE) {
            $this->logger->error($this->db->errorInfo());
            return FALSE;
        }
        if (count($sql["indices"]) > 0) {
            $this->logger->info("Creating indices for table: " . $item_name);
            foreach($sql["indices"] as $s) {
                $this->logger->debug("> > with SQL: " . $s);
                $result = $this->db->exec($s);
                if ($result === FALSE) {
                    $this->logger->error($this->db->errorInfo());
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    private function create_sql($table_name, $field_list) {
        $fields = [];
        $indices = [];
        foreach($field_list as $field => $type) {
            if ($this->is_index($field)) {
                $indices[substr($field, 1)] = $type;
            } else {
                $fields[] = sprintf("%s %s", $field, $this->get_field_type($type));
            }
        }
        $table_sql = sprintf("CREATE TABLE %s (%s)", $table_name, implode(",", $fields));

        $index_sqls = [];
        foreach($indices as $index_name => $index_fields) {
            $index_sql = $this->create_index_sql($table_name, $index_name, $index_fields);
            $index_sqls[] = $index_sql;
        }

        return [
            "table" => $table_sql,
            "indices" => $index_sqls,
        ];
    }

    private function create_index_sql($table_name, $index_name, $index_fields) {
        $index_typed_name = $this->get_index_typed_name($index_name);
        $index_sql = sprintf("CREATE %s ON %s (%s)", $index_typed_name, $table_name, implode(",", $index_fields));
        return $index_sql;
    }

    private function get_index_typed_name($index_name) {
        $unique = FALSE;
        if (preg_match('/(?<name>[a-z]+[a-z0-9_]*)(?<unique>!)?/i', $index_name, $matches)) {
            $index_name = $matches["name"];
            $unique = (array_key_exists("unique", $matches) && $matches["unique"] === "!");
        }
        return trim(sprintf("%s INDEX %s", ($unique ? "UNIQUE" : ""), $index_name));
    }

    private function is_index($name) {
        return substr($name, 0, 1) === "@";
    }

    private function get_field_type($field_type) {
        if (preg_match('/(?<type>[a-z]+[a-z0-9]*)(\((?<length>\d+)\))?(?<autoincrement>\+)?(?<primary>\*)?/i', $field_type, $matches)) {
            $type = $this->find_internal_type($matches["type"]);
            $length = array_key_exists("length", $matches) ? $matches["length"] : "";
            $autoincrement = (array_key_exists("autoincrement", $matches) && $matches["autoincrement"] === "+");
            $primary = (array_key_exists("primary", $matches) && $matches["primary"] === "*");
            if ($length !== "") {
                $type .= sprintf("(%d)", $length);
            }
            if ($primary) {
                $type .= " PRIMARY KEY";
            }
            if ($autoincrement) {
                $type .= " AUTOINCREMENT";
            }
            return $type;
        }
        return $this->find_internal_type($field_type);
    }

    private function find_internal_type($type) {
        $map = [
            "uint" => "UNSIGNED INTEGER",
            "int" => "INTEGER",
        ];

        if (array_key_exists($type, $map)) {
            return $map[$type];
        }
        return strtoupper($type);
    }
}
