<?php

class TemplatesBuilder {
    function __construct($opts) {
        $this->logger = $opts["logger"];
        $this->safe_mode = $opts["safe_mode"];
        $this->var_list = $opts["variables"];
    }

    public function build($blueprint) {
        foreach($blueprint as $item) {
            $result = $this->create($item);
            if ($result === FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    private function create($item) {
        $src = $item[0];
        $dest = $item[1];
        $opts = (count($item) > 2) ? $item[2] : [];

        $this->logger->debug("> Creating " . $dest . " from " . $src . ".");
        $result = TRUE;

        if ($this->is_safe_to_create($dest)) {
            if (file_exists($dest)) {
                if ($this->safe_mode) {
                    $this->logger->info("> File exists in safe mode: " . $dest);
                    $this->logger->info("> Safe mode flag (safe_mode) is used for protecting your current environment in case if you run installation script by accident. Change this flag in setup/config.php to false to avoid this message. When you run installation with this flag as FALSE, all files and data will be reset to initial state.");
                    return FALSE;
                }
                $this->logger->debug("> File exists. Replacing file: " . $dest);
            } else {
                $this->logger->debug("> Creating file: " . $dest);
            }
            $result = $this->compile($src, $dest, $opts);
        } else {
            $this->logger->error("> Creating item outside project is not prohibited.");
            $result = FALSE;
        }

        return $result;
    }

    private function compile($src, $dest, $opts = []) {
        $compiled_content = file_get_contents($src);

        $opts = array_merge($opts, $this->var_list);

        foreach($opts as $name => $value) {
            $filters = [];
            $to_be_replaced = "{{{$name}}}";
            if (preg_match_all('/{{(?<var>' . $name . ')(?<filters>(\|[A-Z0-9_]+)*)}}/i', $compiled_content, $matches, PREG_SET_ORDER)) {
                foreach($matches as $match) {
                    $to_be_replaced = $match[0];
                    $filters = array_values(array_filter(explode("|", $match["filters"]), function($value) { return $value !== ''; }));
                    $value = $this->apply_filters($value, $filters);
                    $compiled_content = str_replace($to_be_replaced, $value, $compiled_content);
                }
            } else {
                $compiled_content = str_replace($to_be_replaced, $value, $compiled_content);
            }
        }

        return file_put_contents($dest, $compiled_content);
    }

    private function apply_filters($value, $filters) {
        foreach ($filters as $filter) {
            $value = $filter($value);
        }
        return $value;
    }

    private function is_safe_to_create($item) {
        if (substr($item, 0, 1) === "/") {
            return FALSE;
        }
        if (substr($item, 0, 2) === "..") {
            return FALSE;
        }
        return TRUE;
    }

}
