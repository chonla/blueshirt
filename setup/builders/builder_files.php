<?php

class FilesBuilder {
    function __construct($opts) {
        $this->logger = $opts["logger"];
        $this->safe_mode = $opts["safe_mode"];
    }

    public function build($blueprint) {
        foreach($blueprint as $item) {
            $result = $this->create($item);
            if ($result === FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    private function is_safe_to_create($item) {
        if (substr($item, 0, 1) === "/") {
            return FALSE;
        }
        if (substr($item, 0, 2) === "..") {
            return FALSE;
        }
        return TRUE;
    }

    private function create($item) {
        $this->logger->info("Creating item: " . $item);
        $result = TRUE;
        if ($this->is_safe_to_create($item)) {
            if ($this->is_dir_name($item)) {
                $this->mkdir_if_not_exist($item);
            } else {
                $result = $this->create_file_anyway($item);
            }
        } else {
            $this->logger->error("> Creating item outside project is not prohibited.");
            $result = FALSE;
        }
        return $result;
    }

    private function mkdir_if_not_exist($dir_name) {
        if (!is_dir($dir_name)) {
            $this->logger->debug("> Creating directory: " . $dir_name);
            mkdir($dir_name);
        } else {
            $this->logger->debug("> Directory exists: " . $dir_name);
        }
    }

    private function create_file_anyway($file_name) {
        if (file_exists($file_name)) {
            if ($this->safe_mode) {
                $this->logger->info("> File exists in safe mode: " . $file_name);
                $this->logger->info("> Safe mode flag (safe_mode) is used for protecting your current environment in case if you run installation script by accident. Change this flag in setup/config.php to false to avoid this message. When you run installation with this flag as FALSE, all files and data will be reset to initial state.");
                return FALSE;
            }
            $this->logger->debug("> File exists. Replacing file: " . $file_name);
            unlink($file_name);
            touch($file_name);
        } else {
            $this->logger->debug("> Creating file: " . $file_name);
        }
    }

    private function is_dir_name($item) {
        return substr($item, -1) === "/";
    }
}
