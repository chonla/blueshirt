<?php

class Builder {
    const DATABASE = "db";
    const FILES = "files";
    const DBDATA = "dbdata";
    const DEPENDENCIES = "dependencies";
    const TEMPLATES = "templates";
    const TESTCASES = "testcases";

    private static $map = [
        self::DATABASE => ["builder_db", "DbBuilder"],
        self::FILES => ["builder_files", "FilesBuilder"],
        self::DBDATA => ["builder_dbdata", "DbDataBuilder"],
        self::DEPENDENCIES => ["builder_dependencies", "DependenciesBuilder"],
        self::TEMPLATES => ["builder_templates", "TemplatesBuilder"],
        self::TESTCASES => ["builder_templates", "TemplatesBuilder"], // reuse the same builder
    ];

    function __construct($opts) {
        $this->safe_mode = $opts["safe_mode"];
    }

    public function set_logger($logger) {
        $this->logger = $logger;
    }

    public function build($type, $options = []) {
        $blueprint = include(dirname(__FILE__)."/../blueprints/${type}.php");

        $this->logger->info("Building: " . $blueprint["name"] . " from blueprint.");

        $target = self::$map[$type];
        include_once $target[0] . ".php";
        $options = array_merge([
            "logger" => $this->logger,
            "safe_mode" => $this->safe_mode,
        ], $options);
        $b = new $target[1]($options);
        return call_user_func_array([$b, "build"], [$blueprint["data"]]);
    }

    public function abort_if_error($result) {
        if ($result === FALSE) {
            $this->logger->info("Installation aborted.");
            exit(1);
        }
    }
}
