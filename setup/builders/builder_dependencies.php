<?php

class DependenciesBuilder {
    function __construct($opts) {
        $this->logger = $opts["logger"];
        $this->blueshirt = $opts["blueshirt"];
    }

    public function build($blueprints) {
        $this->logger->info("Creating dependencies: composer.json");
        
        $this->require = $blueprints["require"];
        $this->require_dev = $blueprints["require_dev"];

        $composer_config = [
            "name" => $this->blueshirt["name"],
            "version" => $this->blueshirt["version"],
            "author" => $this->blueshirt["author"],
            "autoload" => [
                "psr-4" => [
                    "App\\" => "src",
                ],
            ],
        ];

        if ($this->require !== []) {
            $composer_config["require"] = $this->require;
        }
        if ($this->require_dev !== []) {
            $composer_config["require-dev"] = $this->require_dev;
        }

        $result = file_put_contents("composer.json", json_encode($composer_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        if ($result === FALSE) {
            return FALSE;
        }
        return TRUE;
    }
}
