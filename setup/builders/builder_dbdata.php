<?php

class DbDataBuilder {
    function __construct($opts) {
        $this->logger = $opts["logger"];
        $this->dsn = $opts["dsn"];
    }

    public function build($blueprint) {
        $this->logger->info("Connecting to datbase: " . $this->dsn);
        $this->db = new PDO($this->dsn);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        foreach($blueprint as $table => $rows) {
            $result = $this->multiple_insert($table, $rows);
            if ($result === FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    private function multiple_insert($table, $rows) {
        $this->logger->debug("> Table: " . $table);
        foreach($rows as $row) {
            $stmt = $this->create_insert_statement($table, $row);

            foreach($row as $name => $value) {
                $stmt->bindValue(sprintf(":%s", $name), $value);
            }
            $this->logger->debug("> > Inserting data: " . json_encode($row));
            $result = $stmt->execute();
            if ($result === FALSE) {
                return FALSE;
            }
        }
        return TRUE;
    }

    private function create_insert_statement($table, $row) {
        $names = array_keys($row);
        $values = array_values($row);

        $sql = sprintf("INSERT INTO $table (%s) VALUES (%s)", implode(",", $names), ":".implode(",:", $names));
        $stmt = $this->db->prepare($sql);
        return $stmt;
    }
}
