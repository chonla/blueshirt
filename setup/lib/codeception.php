<?php

class Codeception {
    function __construct($opts) {
        $this->codeception_url = $opts["url"];
        if (version_compare(PHP_VERSION, '5.6', '<')) {
            $this->codeception_url = $opts["php54_url"];
        }
    }

    public function set_logger($logger) {
        $this->logger = $logger;
    }

    public function set_tmp_dir($dir) {
        $this->tmp_dir = $dir;
    }

    public function set_downloader($downloader) {
        $this->downloader = $downloader;
    }

    public function cleanup() {
        $this->logger->info("Codeception: Cleanup codeception copy.");
        $this->delTree(__DIR__.'/../../tests');
        unlink(__DIR__.'/../../codeception.yml');
    }

    private function delTree($dir) {
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public function install() {
        $this->logger->info("Codeception: Installing codeception.");
        $codecept = $this->tmp_dir . "./codecept.phar";

        $this->logger->info("> Retrieving from " . $this->codeception_url . ".");
        $result = $this->downloader->download($this->codeception_url, $codecept);

        if ($result === FALSE) {
            return FALSE;
        }

        $output = [];
        $exit_code = 0;
        $invoke_command = sprintf('php %s bootstrap', $codecept);
        exec($invoke_command, $output, $exit_code);

        $result = ($exit_code === 0);
        if ($result == FALSE) {
            $this->logger->info("> Unable to generate test suite.");
        }

        return $result;
    }

    public function generate_test_suite() {
        $this->logger->info("Codeception: Generating test suite.");
        $codecept = $this->tmp_dir . "./codecept.phar";

        $output = [];
        $exit_code = 0;
        $invoke_command = sprintf('php %s generate:suite api', $codecept);
        exec($invoke_command, $output, $exit_code);

        $result = ($exit_code === 0);
        if ($result == FALSE) {
            $this->logger->info("> Unable to generate test suite.");
        }

        return $result;
    }
}
