<?php

class Logger {
    private static $level = [
        "ALL" => 0,
        "DEBUG" => 1,
        "INFO" => 2,
        "WARN" => 3,
        "ERROR" => 4,
        "FATAL" => 5,
        "OFF" => 99,
    ];

    function __construct() {
        $this->current_level = self::$level["ALL"];
    }

    public function set_level($level) {
        $this->current_level = self::$level[$level];
    }

    public function nl() {
        if ($this->current_level !== self::$level["OFF"]) {
            printf("\n");
        }
    }

    public function info($msg) {
        $this->write("INFO", $msg);
    }

    public function debug($msg) {
        $this->write("DEBUG", $msg);
    }

    public function error($msg) {
        $this->write("ERROR", $msg);
    }

    private function write($type, $msg) {
        if ($this->current_level <= self::$level[$type]) {
            printf("%-5s %s\n", $type, $msg);
        }
    }
}
