<?php

class BlueShirt {
    function __construct($opts) {
        $this->opts = $opts;
    }

    public function set_logger($logger) {
        $this->logger = $logger;
    }

    public function initialize() {
        $this->logger->info("Initialize: BlueShirt");
        $this->logger->nl();
    }

    public function install() {
        $builder = new Builder($this->opts["builder"]);
        $builder->set_logger($this->logger);

        $result = $builder->build(Builder::FILES);
        $this->logger->nl();
        $builder->abort_if_error($result);

        $result = $builder->build(Builder::DATABASE, $this->opts["database"]);
        $this->logger->nl();
        $builder->abort_if_error($result);

        $result = $builder->build(Builder::DBDATA, $this->opts["database"]);
        $this->logger->nl();
        $builder->abort_if_error($result);

        $downloader = new Downloader();
        $composer = new Composer($this->opts["composer"]);
        $composer->set_tmp_dir($this->opts["blueshirt"]["tmp_dir"]);
        $composer->set_logger($this->logger);
        $composer->set_downloader($downloader);

        $result = $composer->install();
        $this->logger->nl();
        $builder->abort_if_error($result);

        $result = $builder->build(Builder::DEPENDENCIES, ["blueshirt" => $this->opts["blueshirt"]]);
        $this->logger->nl();
        $builder->abort_if_error($result);

        $result = $composer->install_dependencies();
        $this->logger->nl();
        $builder->abort_if_error($result);

        $var_list = [
            "DSN" => $this->opts["database"]["dsn"],
        ];
        $result = $builder->build(Builder::TEMPLATES, ["variables" => $var_list]);
        $this->logger->nl();
        $builder->abort_if_error($result);

        if ($this->opts["blueshirt"]["install_test"]) {
            $codeception = new Codeception($this->opts["codeception"]);
            $codeception->set_tmp_dir($this->opts["blueshirt"]["tmp_dir"]);
            $codeception->set_logger($this->logger);
            $codeception->set_downloader($downloader);

            $codeception->cleanup();

            $result = $codeception->install();
            $this->logger->nl();
            $builder->abort_if_error($result);

            $result = $codeception->generate_test_suite();
            $this->logger->nl();
            $builder->abort_if_error($result);

            $var_list = [
                "REST_BASE_URL" => $this->opts["codeception"]["base_url"],
            ];
            $result = $builder->build(Builder::TESTCASES, ["variables" => $var_list, "safe_mode" => false ]);
            $this->logger->nl();
            $builder->abort_if_error($result);
        }

        return TRUE;
    }
}
