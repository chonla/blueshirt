<?php

class Composer {
    function __construct($opts) {
        $this->composer_url = $opts["url"];
        $this->composer_sig_url = $opts["signature"];
    }

    public function set_logger($logger) {
        $this->logger = $logger;
    }

    public function set_tmp_dir($dir) {
        $this->tmp_dir = $dir;
    }

    public function set_downloader($downloader) {
        $this->downloader = $downloader;
    }

    public function install() {
        $this->logger->info("Composer: Installing composer.");
        $tmp_file = $this->tmp_dir . "./composer_install.php";

        $result = FALSE;

        if (file_exists($tmp_file)) {
            $this->logger->info("> Previous copy of composer found.");

            $result = $this->verify_download($tmp_file);

            if ($result === FALSE) {
                $this->logger->info("> The composer copy is no longer valid and has been discarded.");
            }
        }

        if ($result === FALSE) {
            $this->logger->info("> Retrieving from " . $this->composer_url . ".");
            $result = $this->downloader->download($this->composer_url, $tmp_file);

            if ($result === FALSE) {
                return FALSE;
            }

            $result = $this->verify_download($tmp_file);
        }

        $output = [];
        $exit_code = 0;
        $invoke_command = sprintf('php %s --quiet --install-dir="%s"', $tmp_file, $this->tmp_dir);
        exec($invoke_command, $output, $exit_code);

        $result = ($exit_code === 0);

        if ($result == FALSE) {
            $this->logger->info("> Unable to download Composer.");
        }

        return $result;
    }

    public function install_dependencies() {
        $this->logger->info("Composer: Installing dependencies");
        $this->logger->info("> Downloading dependencies from composer.json.");

        $composer = $this->tmp_dir . "./composer.phar";
        $output = [];
        $exit_code = 0;
        $invoke_command = sprintf('php %s install', $composer);
        exec($invoke_command, $output, $exit_code);

        $result = ($exit_code === 0);

        if ($result == FALSE) {
            $this->logger->info("> Unable to download dependencies.");
        }

        return $result;
    }

    private function verify_download($downloaded_file) {
        $this->logger->info("> Verifying with composer signature from " . $this->composer_sig_url . ".");
        $expected_sig = trim($this->downloader->get_remote_content($this->composer_sig_url));
        $actual_sig = $this->get_download_signature($downloaded_file);

        $result = ($expected_sig === $actual_sig);
        if ($result === FALSE) {
            $this->logger->info("> Error: Hash verification failed. Expect " . $expected_sig . " but got " . $actual_sig . ".");
            $this->logger->info("> Remove downloaded file.");
            unlink($downloaded_file);
        }
        return $result;
    }

    private function get_download_signature($downloaded_file) {
        return hash_file('SHA384', $downloaded_file);
    }
}
