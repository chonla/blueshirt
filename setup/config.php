<?php

return [
    "blueshirt" => [
        "name" => "BlueShirt",
        "version" => "0.1",
        "author" => [
            "name" => "Chonlasith Jucksriporn",
            "email" => "chonlasith@gmail.com",
        ],
        "tmp_dir" => "setup/tmp/",
        "base_dir" => "setup/",
        "log_level" => "ALL",
        "install_test" => true,
    ],
    "builder" => [
        "safe_mode" => false,
    ],
    "database" => [
        "dsn" => "sqlite:./db/db.sq3",
        "username" => "",
        "password" => "",
    ],
    "composer" => [
        "url" => "https://getcomposer.org/installer",
        "signature" => "https://composer.github.io/installer.sig",
    ],
    "codeception" => [
        "url" => "http://codeception.com/codecept.phar",
        "php54_url" => "http://codeception.com/php54/codecept.phar",
        "base_url" => "http://localhost:8000/",
    ],
];
