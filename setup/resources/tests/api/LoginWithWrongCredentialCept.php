<?php

$I = new ApiTester($scenario);
$I->wantTo('login');
$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
$I->sendPOST('/auth/login', ['login' => 'someuser', 'password' => 'this_is_not_correct_password']);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED);
