<?php

$I = new ApiTester($scenario);
$I->wantTo('get article list without login');
$I->sendGet('/articles/1');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
