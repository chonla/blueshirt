<?php

$I = new ApiTester($scenario);
$I->wantTo('login');
$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
$I->sendPOST('/auth/login', ['login' => 'admin', 'password' => 'admin123456']);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsJson();
$I->seeResponseJsonMatchesJsonPath('$.access_token');
