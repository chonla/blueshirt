<?php

$I = new ApiTester($scenario);
$I->wantTo('get article list');
$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
$I->sendPOST('/auth/login', ['login' => 'admin', 'password' => 'admin123456']);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsJson();
$I->seeResponseJsonMatchesJsonPath('$.access_token');
$access_token = $I->grabDataFromResponseByJsonPath('$.access_token');
$I->sendGet('/articles?access_token=' . $access_token[0]);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsJson();
$I->seeResponseJsonMatchesJsonPath('$._attributes.row_count');
$I->seeResponseJsonMatchesJsonPath('$.results[0]');
