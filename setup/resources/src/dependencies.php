<?php

$container = $app->getContainer();

// redbean
$container['r'] = function ($c) {
    $settings = $c->get('settings')['r'];
    $r = new \RedBeanPHP\R();
    $r->setup($settings['dsn']);
    return $r;
};

// randomizr
$container['randomizr'] = function ($c) {
    $r = new \Chonla\Randomizr\Randomizr();
    return $r;
};

// services
$container['access_token_service'] = function ($c) {
    $r = new App\Services\AccessTokenService($c);
    return $r;
};

$container['authenticator_service'] = function ($c) {
    $r = new App\Services\AuthenticatorService($c);
    return $r;
};

// middlewares
$container['access_token_middleware'] = function ($c) {
    $r = new App\Middlewares\AccessTokenMiddleware($c);
    return $r;
};
