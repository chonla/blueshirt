<?php

namespace App\Middlewares;

class AccessTokenMiddleware {
    function __construct(\Slim\Container $ci) {
        $this->ci = $ci;
    }

    public function __invoke($request, $response, $next) {
        // Before
        $access_token = $request->getQueryParam('access_token');
        $access_token_service = $this->ci->get('access_token_service');
        if (($result = $access_token_service->verify($access_token)) !== FALSE) {
            $response = $next($request, $response);
        } else {
            $response = $response->withStatus(401);
        }
        // After

        return $response;
    }
}
