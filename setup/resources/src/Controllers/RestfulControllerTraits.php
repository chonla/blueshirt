<?php

namespace App\Controllers;

trait RestfulControllerTraits {
    private $method_map = [
        self::ELEMENT => [
            'GET' => 'doGetElement',
            'POST' => 'doPostElement',
        ],
        self::COLLECTION => [
            'GET' => 'doGetCollection',
            'POST' => 'doPostCollection',
        ],
    ];

    function __construct(\Slim\Container $ci) {
        $this->ci = $ci;
        $this->conf = $this->ci->get('settings')['restful'];
    }

    public function __invoke($request, $response, $args) {
        $access_level = self::COLLECTION;
        if (array_key_exists('id', $args)) {
            $access_level = self::ELEMENT;
        }
        $callable = [$this, $this->method_map[$access_level][$request->getMethod()]];
        return call_user_func_array($callable, [$request, $response, $args]);
    }

    private function doGetElement($request, $response, $args) {
        $id = $args['id'];
        $R = $this->ci->get('r');
        $element = $R->findOne($this->table, 'id=:id', [':id'=>$id]);
        return $response->withJson($element);
    }

    private function doGetCollection($request, $response, $args) {
        $R = $this->ci->get('r');

        $current_page = $request->getQueryParam('page', '1');
        if (!preg_match('/^\d+$/', $current_page)) {
            $current_page = '1';
        }

        $current_page = intVal($current_page);
        $count = $R->count($this->table);
        $page_size = $this->conf['page_size'];
        $page_count = 1;
        if ($count > 0) {
            $page_count = ceil($count / $page_size);
        }
        $from_row = (($current_page - 1) * $page_size);
        $api_url = strtok($request->getUri()->__toString(), '?');

        $all = array_values($R->findAll($this->table, sprintf(' ORDER BY id LIMIT %d OFFSET %d ', $page_size, $from_row)));

        $attributes = [
            'row_count' => $count,
            'page_count' => $page_count,
            'current_page' => $current_page,
            'page_size' => $page_size,
            'first_page' => sprintf('%s?page=%d', $api_url, '1'),
            'last_page' => sprintf('%s?page=%d', $api_url, $page_count),
        ];

        if ($current_page < $page_count) {
            $next_page = $current_page + 1;
            $attributes['next_page'] = sprintf('%s?page=%d', $api_url, $next_page);
        }

        if ($current_page > 1) {
            $previous_page = $current_page - 1;
            $attributes['previous_page'] = sprintf('%s?page=%d', $api_url, $previous_page);;
        }

        return $response->withJson([
            '_attributes' => $attributes,
            'results' => $all,
        ]);
    }

    private function doPostElement($request, $response, $args) {

    }

    private function doPostCollection($request, $response, $args) {

    }
}
