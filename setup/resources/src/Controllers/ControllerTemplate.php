<?php

namespace App\Controllers;

class {{MODEL|strtolower|ucfirst}}Controller implements RestfulControllerInterface {
    use \App\Controllers\RestfulControllerTraits;

    private $table = "{{MODEL|strtolower}}";
}
