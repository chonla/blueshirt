<?php

namespace App\Controllers;

interface RestfulControllerInterface {
    const ELEMENT = "ELEMENT";
    const COLLECTION = "COLLECTION";
}
