<?php

namespace App\Controllers;

class AuthenController {
    function __construct(\Slim\Container $ci) {
        $this->ci = $ci;
    }

    public function doLogin($request, $response, $args) {
        $parsedBody = $request->getParsedBody();
        $login = $parsedBody['login'];
        $password = $parsedBody['password'];

        $authenticator_service = $this->ci->get('authenticator_service');
        if (($login_info = $authenticator_service->login($login, $password)) !== FALSE) {
            $access_token_service = $this->ci->get('access_token_service');
            $access_token = $access_token_service->pack($login_info);

            return $response->withJson([
                'access_token' => $access_token,
            ]);
        }
        return $response->withStatus(401);
    }
}
