<?php

namespace App\Services;

class AuthenticatorService {
    function __construct(\Slim\Container $ci) {
        $this->ci = $ci;
    }

    public function login($login, $password) {
        $R = $this->ci->get('r');
        $user = $R->findOne('users', 'login=:login', [':login'=>$login]);
        if ($user !== NULL) {
            if (password_verify($password, $user['pwd'])) {
                return $user;
            }
        }
        return FALSE;
    }
}
