<?php

namespace App\Services;

class AccessTokenService {
    function __construct(\Slim\Container $ci) {
        $this->ci = $ci;
    }

    public function verify($access_token) {
        $payload_plain = base64_decode($access_token, true);
        if ($payload_plain !== FALSE) {
            $payload = json_decode($payload_plain, true);

            $user_id = $payload['user_id'];
            $pin = $payload['pin'];
            $signature = $payload['signature'];

            $R = $this->ci->get('r');
            $login_info = $R->findOne('users', 'id=:id', [':id'=>$user_id]);

            if ($login_info !== NULL) {
                $expected_signature = $this->sign($login_info['id'], $pin, $login_info['secret']);
                return hash_equals($expected_signature, $signature);
            }
        }
        return FALSE;
    }

    public function pack($login_info) {
        $pin = $this->ci->get('randomizr')->alphanumeric(16);
        $signature = $this->sign($login_info['id'], $pin, $login_info['secret']);
        return base64_encode(json_encode([
            'user_id' => $login_info['id'],
            'pin' => $pin,
            'signature' => $signature,
        ]));
    }

    private function sign($user_id, $pin, $key) {
        $message = sprintf('%s.%s.%s', $user_id, $pin, $key);
        return hash_hmac('sha256', $message, $key);
    }
}
