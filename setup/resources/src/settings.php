<?php

return [
    'settings' => [
        'r' => [
            'dsn' => '{{DSN}}',
        ],
        'restful' => [
            'page_size' => 30,
        ],
    ],
];
