<?php

$app->get('/', function ($request, $response, $args) {
    echo "Hello";
});

$app->group('/auth', function() use ($app) {
    $app->post('/login', 'App\Controllers\AuthenController:doLogin');
});

$app->any('/articles[/{id}]', 'App\Controllers\ArticlesController')
    ->add('access_token_middleware');
