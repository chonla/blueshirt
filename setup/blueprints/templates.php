<?php

return [
    "name" => "Templates",
    "data" => [
        ["setup/resources/index.php", "index.php"],
        ["setup/resources/src/settings.php", "src/settings.php"],
        ["setup/resources/src/dependencies.php", "src/dependencies.php"],
        ["setup/resources/src/middleware.php", "src/middleware.php"],
        ["setup/resources/src/routes.php", "src/routes.php"],
        ["setup/resources/src/Controllers/RestfulControllerTraits.php", "src/Controllers/RestfulControllerTraits.php"],
        ["setup/resources/src/Controllers/RestfulControllerInterface.php", "src/Controllers/RestfulControllerInterface.php"],
        ["setup/resources/src/Controllers/ControllerTemplate.php", "src/Controllers/ArticlesController.php", [
            "MODEL" => "articles",
        ]],
        ["setup/resources/src/Controllers/AuthenController.php", "src/Controllers/AuthenController.php"],
        ["setup/resources/src/Middlewares/AccessTokenMiddleware.php", "src/Middlewares/AccessTokenMiddleware.php"],
        ["setup/resources/src/Services/AccessTokenService.php", "src/Services/AccessTokenService.php"],
        ["setup/resources/src/Services/AuthenticatorService.php", "src/Services/AuthenticatorService.php"],
    ],
];
