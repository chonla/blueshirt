<?php

return [
    "name" => "Dependencies",
    "data" => [
        "require" => [
            "slim/slim" => "^3.0",
            "gabordemooij/redbean" => "dev-master",
            "chonla/randomizr" => "dev-master",
        ],
        "require_dev" => [],
    ],
];
