<?php

return [
    "name" => "Files",
    "data" => [
        "setup/tmp/",
        "db/",
        "db/db.sq3",
        "src/",
        "src/Controllers/",
        "src/Middlewares/",
        "src/Services/"
    ],
];
