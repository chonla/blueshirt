<?php

return [
    "name" => "DB",
    "data" => [
        "users" => [
            "login" => "varchar(200)",
            "pwd" => "char(60)",
            "active" => "tinyint",
            "secret" => "char(32)",
            "@user_login!" => ["login"],
            "@user_authen!" => ["login", "pwd"],
        ],
        "articles" => [
            "title" => "varchar(400)",
            "body" => "text",
            "owner" => "int",
            "@article_owner" => ["owner"],
        ],
    ],
];
