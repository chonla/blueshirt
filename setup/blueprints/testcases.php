<?php

return [
    "name" => "Testcases",
    "data" => [
        ["setup/resources/tests/api.suite.yml", "tests/api.suite.yml"],
        ["setup/resources/tests/api/LoginCept.php", "tests/api/LoginCept.php"],
        ["setup/resources/tests/api/LoginWithWrongCredentialCept.php", "tests/api/LoginWithWrongCredentialCept.php"],
        ["setup/resources/tests/api/GetArticlesCept.php", "tests/api/GetArticlesCept.php"],
        ["setup/resources/tests/api/GetArticlesWithoutLoginCept.php", "tests/api/GetArticlesWithoutLoginCept.php"],
        ["setup/resources/tests/api/GetArticlesWithWrongAccessTokenCept.php", "tests/api/GetArticlesWithWrongAccessTokenCept.php"],
        ["setup/resources/tests/api/GetSingleArticleCept.php", "tests/api/GetSingleArticleCept.php"],
        ["setup/resources/tests/api/GetSingleArticleWithoutLoginCept.php", "tests/api/GetSingleArticleWithoutLoginCept.php"],
        ["setup/resources/tests/api/GetSingleArticleWithoutWrongAccessTokenCept.php", "tests/api/GetSingleArticleWithoutWrongAccessTokenCept.php"],
    ],
];
