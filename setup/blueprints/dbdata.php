<?php

return [
    "name" => "DB Data",
    "data" => [
        "users" => [
            [
                "login" => "admin",
                "pwd" => password_hash("admin123456", PASSWORD_BCRYPT),
                "secret" => "_PUT_ANY_TEXT_HERE_",
                "active" => 1,
                "created_time" => time(),
                "updated_time" => time(),
            ],
        ],
        "articles" => [
            [
                "title" => "Welcome",
                "body" => "Welcome to BlueShirt.",
                "owner" => 1,
                "created_time" => time(),
                "updated_time" => time(),
            ],
            [
                "title" => "BlueShirt is awesome!",
                "body" => "BlueShirt is easy.",
                "owner" => 1,
                "created_time" => time(),
                "updated_time" => time(),
            ],
        ],
    ],
];
